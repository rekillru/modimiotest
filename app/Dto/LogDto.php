<?php
namespace App\Dto;

use Illuminate\Http\Request;

class LogDto
{
    public int $page;

    public int $perPage;

    public array $filters = [];

    public string $sort = '';

    public array $aggregate = [];

    public array $groupBy = [];

    public array $paginationAppends = [];

    public function __construct(Request $request)
    {
        $this->page = $request->get('page', 1);
        $this->perPage = env('PER_PAGE');
        $this->filters = $request->only('dateFrom', 'dateTo', 'operation_system', 'architecture');
        $this->sort = $request->get('sort', '');
        $this->paginationAppends = $request->only('sort', 'dateFrom', 'dateTo', 'operation_system', 'architecture');
    }

    public function setAggregate(array $aggregate)
    {
        $this->aggregate = $aggregate;
    }

    public function setGroupBy(array $aggregate)
    {
        $this->aggregate = $aggregate;
    }

    public function setSort(string $sortField)
    {
        $this->sort = $sortField;
    }
}
