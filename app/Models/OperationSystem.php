<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Операционная система
 *
 * @property int $id
 * @property string $name
 */
class OperationSystem extends Model
{
    protected $table = 'operation_systems';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
