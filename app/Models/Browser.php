<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Браузер
 *
 * @property int $id
 * @property string $name
 */
class Browser extends Model
{
    protected $table = 'browsers';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
