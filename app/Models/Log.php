<?php

namespace App\Models;

use App\Traits\LogRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Лог
 *
 * @property int $id
 * @property string $ip
 * @property Carbon $datetime
 * @property string $url
 * @property string $user_agent
 * @property string $architecture
 * @property int $operation_system_id
 * @property int $browser_id
 */
class Log extends Model
{
    use LogRepository;

    const ARCHITECTURE_X86 = 'x86';
    const ARCHITECTURE_X64 = 'x64';

    protected $table = 'logs';

    public $timestamps = false;

    protected $fillable = [
        'ip',
        'datetime',
        'url',
        'user_agent',
        'architecture',
        'operation_system_id',
        'browser_id',
    ];

    /**
     * Операционная система
     *
     * @return BelongsTo
     */
    public function operation_system(): BelongsTo
    {
        return $this->belongsTo(OperationSystem::class);
    }

    /**
     * Браузер
     *
     * @return BelongsTo
     */
    public function browser(): BelongsTo
    {
        return $this->belongsTo(Browser::class);
    }
}
