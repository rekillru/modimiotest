<?php

namespace App\Traits;

use App\Dto\LogDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait LogRepository
{
    public function scopeQueryBuilder(Builder $query, LogDto $dto): Builder
    {
        $fromSub = static::query()->selectRaw('DATE(datetime) date')->groupBy('date');
        $query->addSelect('logs_table.date')
            ->fromSub($fromSub->toSql(), 'logs_table')
            ->setBindings(array_merge($query->getBindings(), $fromSub->getBindings()));;
        foreach ($dto->aggregate as $aggregateField) {
            $aggregateMethodName = 'aggregate' . Str::studly($aggregateField);
            if (method_exists($this, 'scope' . $aggregateMethodName)) {
                $query->$aggregateMethodName($dto);
            }
        }
        $query->sort($dto->sort);

        return $query;
    }

    public function scopeAggregatePopularBrowserName(Builder $query, LogDto $dto): Builder
    {
        $fromSub = static::query()
            ->selectRaw('DATE(logs.datetime) date, COUNT(logs.id) popular_browser_count, browsers.name popular_browser_name')
            ->leftJoin('browsers', 'browsers.id', 'logs.browser_id')
            ->groupBy('date', 'browser_id')
            ->filter($dto->filters);
        $subQuery = static::query()
            ->select('s.*')
            ->fromSub($fromSub->toSql(), 's')
            ->leftJoin(DB::raw('(' . $fromSub->toSql() . ') lj'), fn ($q) =>
                $q->on('s.date', 'lj.date')->on('s.popular_browser_count', '<', 'lj.popular_browser_count'))
            ->whereRaw('lj.popular_browser_count is null')
            ->setBindings(array_merge($fromSub->getBindings(), $fromSub->getBindings()));
        $query->addSelect('pb.popular_browser_count', 'pb.popular_browser_name')
            ->join(DB::raw('(' . $subQuery->toSql() . ') pb'), 'pb.date', 'logs_table.date')
            ->setBindings(array_merge($query->getBindings(), $subQuery->getBindings()));

        return $query;
    }

    public function scopeAggregatePopularUrl(Builder $query, LogDto $dto): Builder
    {
        $fromSub = static::query()
            ->selectRaw('DATE(datetime) date, COUNT(id) url_count, url')
            ->groupBy('date', 'url')
            ->filter($dto->filters);
        $subQuery = static::query()
            ->select('s.*')
            ->fromSub($fromSub->toSql(), 's')
            ->leftJoin(DB::raw('(' . $fromSub->toSql() . ') lj'), fn ($q) =>
                $q->on('s.date', 'lj.date')->on('s.url_count', '<', 'lj.url_count'))
            ->whereRaw('lj.url_count is null')
            ->setBindings(array_merge($fromSub->getBindings(), $fromSub->getBindings()));
        $query->addSelect(DB::raw('pu.url popular_url, pu.url_count'))
            ->join(DB::raw('(' . $subQuery->toSql() . ') pu'), 'pu.date', 'logs_table.date')
            ->setBindings(array_merge($query->getBindings(), $subQuery->getBindings()));

        return $query;
    }

    public function scopeAggregateRequestCount(Builder $query, LogDto $dto): Builder
    {
        $subQuery = static::query()
            ->selectRaw('DATE(datetime) date, COUNT(id) request_count')
            ->groupBy('date')
            ->filter($dto->filters);
        $query->addSelect('rc.request_count')
            ->join(DB::raw('(' . $subQuery->toSql() . ') rc'), 'rc.date', 'logs_table.date')
            ->setBindings(array_merge($query->getBindings(), $subQuery->getBindings()));

        return $query;
    }

    public function scopeAggregatePopularBrowserNames(Builder $query, LogDto $dto): Builder
    {
        $popularBrowserIds = static::query()
            ->selectRaw('COUNT(logs.id) popular_browser_count, browser_id')
            ->groupBy('browser_id')
            ->filter($dto->filters)
            ->orderBy('popular_browser_count', 'desc')
            ->limit(3)
            ->pluck('browser_id');
        $subQuery = static::query()
            ->selectRaw('DATE(logs.datetime) date, COUNT(logs.id) popular_browser_count, browsers.name popular_browser_name')
            ->whereIn('logs.browser_id', $popularBrowserIds)
            ->leftJoin('browsers', 'browsers.id', 'logs.browser_id')
            ->groupBy('date', 'browser_id')
            ->filter($dto->filters)
            ->orderBy('popular_browser_name');
        $query->addSelect('pb.popular_browser_count', 'pb.popular_browser_name')
            ->join(DB::raw('(' . $subQuery->toSql() . ') pb'), 'pb.date', 'logs_table.date')
            ->setBindings(array_merge($query->getBindings(), $subQuery->getBindings()));

        return $query;
    }

    public function scopeFilter(Builder $query, array $filters): Builder
    {
        foreach ($filters as $filterName => $filterValue) {
            if (empty($filterValue)) {
                continue;
            }
            $filterMethodName = 'filter' . Str::studly($filterName) . 'Attribute';
            if (method_exists($this, $filterMethodName)) {
                $query = $this->$filterMethodName($query, $filterValue);
            } else if (in_array($filterName, $this->fillable)) {
                $query = $query->where($filterName, $filterValue);
            }
        }

        return $query;
    }

    public function filterDateFromAttribute(Builder $query, $value)
    {
        $date = Carbon::create($value)->startOfDay();

        return $query->where('datetime', '>=', $date);
    }

    public function filterDateToAttribute(Builder $query, $value)
    {
        $date = Carbon::create($value)->endOfDay();

        return $query->where('datetime', '<', $date);
    }

    public function filterOperationSystemAttribute(Builder $query, $value)
    {
        return $query->where('operation_system_id', $value);
    }

    public function scopeSort(Builder $query, string $sortField): Builder
    {
        switch ($sortField) {
            case 'date':
                $query->orderBy('logs_table.date');
                break;
            case 'request_count':
                $query->orderBy('rc.request_count');
                break;
            case 'popular_url':
                $query->orderBy('popular_url');
                break;
            case 'popular_browser':
                $query->orderBy('pb.popular_browser_name');
                break;
        }

        return $query;
    }
}
