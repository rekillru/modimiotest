<?php

namespace App\Services;

use App\Models\Browser;
use App\Models\Log;
use App\Models\OperationSystem;
use BenMorel\ApacheLogParser\Parser as ApacheLogParser;
use Carbon\Carbon;
use foroco\BrowserDetection;

class LogService
{
    const LOG_FORMAT = '%h %l %u %t "%m %U %H" %>s %B "%{Referer}i" "%{User-Agent}i"';

    protected ApacheLogParser $apacheLogParser;

    protected BrowserDetection $userAgentParser;

    public function __construct(BrowserDetection $userAgentParser)
    {
        $this->apacheLogParser = new ApacheLogParser(self::LOG_FORMAT);
        $this->userAgentParser = $userAgentParser;
    }

    public function create(array $logs)
    {
        Log::upsert($logs, ['ip', 'datetime', 'url']);
    }

    public function parse(string $log): array
    {
        $logData = $this->apacheLogParser->parse($log, false);
        $device = $this->userAgentParser->getAll($logData[10]);
        $operationSystem = OperationSystem::firstOrCreate(['name' => $device['os_title']]);
        $browser = Browser::firstOrCreate(['name' => $device['browser_title']]);

        return [
            'ip' => $logData[0],
            'datetime' => Carbon::create($logData[3]),
            'url' => urldecode($logData[5]),
            'user_agent' => $logData[10],
            'architecture' => $device['64bits_mode'] ? Log::ARCHITECTURE_X64 : Log::ARCHITECTURE_X86,
            'operation_system_id' => $operationSystem->id,
            'browser_id' => $browser->id,
        ];
    }
}
