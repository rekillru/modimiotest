<?php

namespace App\Console\Commands;

use App\Services\LogService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class ParseLog extends Command
{
    const PROGRESS_FORMAT = "[%message%]\n[%bar%] %current%/%max% %percent:3s%% %elapsed:6s%/%estimated:-6s%";
    const BLOCK_LIMIT_LINES = 500;

    protected LogService $logService;

    public function __construct(LogService $logService)
    {
        parent::__construct();
        $this->logService = $logService;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсинг лог-файла';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info($this->description);
        $disk = Storage::disk('public');
        if (!$disk->exists(env('SERVER_LOG_PATH'))) {
            $this->error("Не найден файл лога. Укажите путь к файлу в переменной окружения SERVER_LOG_PATH.");
            return;
        }
        $file = $disk->readStream(env('SERVER_LOG_PATH'));
        $filesize = fstat($file)['size'];
        $this->progressBar = $this->output->createProgressBar();
        $this->progressBar->setFormat(self::PROGRESS_FORMAT);
        $this->progressBar->setMessage("Размер лога (байт): ${filesize}");
        $this->progressBar->start($filesize);
        $blockLimiter = self::BLOCK_LIMIT_LINES;
        $block = [];
        $validLinesCount = 0;
        $invalidLinesCount = 0;
        while (!feof($file)) {
            $line = fgets($file);
            try {
                $block []= $this->logService->parse($line);
                $validLinesCount++;
            } catch (\InvalidArgumentException $e) {
                $invalidLinesCount++;
            }
            $blockLimiter--;
            $this->progressBar->advance(mb_strlen($line));
            if ($blockLimiter == 0) {
                $this->logService->create($block);
                $blockLimiter = self::BLOCK_LIMIT_LINES;
                $block = [];
            }
        }
        Cache::flush();
        $this->progressBar->finish();
        $this->line('');
        $this->info("Строк обработано: $validLinesCount");
        $this->error("Строк с неверным форматом: $invalidLinesCount");
        $this->info("Готово!");
    }
}
