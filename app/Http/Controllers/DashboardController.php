<?php

namespace App\Http\Controllers;

use App\Dto\LogDto;
use App\Models\Log;
use App\Models\OperationSystem;
use Illuminate\Support\Facades\Cache;

class DashboardController extends Controller
{
    public function index(LogDto $dto)
    {
        $url = request()->fullUrl();
        $dto->setAggregate(['popular_browser_name', 'popular_url', 'request_count']);
        $logs = Cache::rememberForever($url . 'table', fn () => Log::queryBuilder($dto)->get());
        $dto->setAggregate(['popular_browser_names']);
        $dto->setSort('');
        $browserNamesLogs = Cache::rememberForever($url . 'charts', fn () =>
            Log::queryBuilder($dto)->get());
        $browserNamesChartGroupedByDates = $browserNamesLogs->groupBy('date');
        $browserNamesChartDates = $browserNamesChartGroupedByDates->keys()->sort();
        $browserNamesChartBrowsers = $browserNamesLogs->pluck('popular_browser_name')->filter()->unique()->sort();
        $browserNamesChartRequestCounts = $browserNamesChartBrowsers->mapWithKeys(fn ($browserName) =>
            [$browserName => $browserNamesChartGroupedByDates->map(fn ($group) =>
                $group->firstWhere('popular_browser_name', $browserName)->popular_browser_count ?? 0)]);
        $requestCountChart = $logs->pluck('request_count', 'date')->sortKeys();
        $operationSystems = OperationSystem::pluck('name', 'id');
        $logsForPage = $logs->paginate($dto->perPage)->appends($dto->paginationAppends);

        return view('dashboard', [
            'logs' => $logs,
            'logsForPage' => $logsForPage,
            'requestCountChart' => $requestCountChart,
            'browserChart' => [
                'dates' => $browserNamesChartDates->toJson(),
                'browser_names' => $browserNamesChartBrowsers,
                'request_count' => $browserNamesChartRequestCounts,
            ],
            'operationSystems' => $operationSystems,
        ]);
    }
}
