<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->string('url', 720)->change();
            $table->string('user_agent', 720)->change();
            $table->dateTime('datetime')->change();
            $table->unique([
                'ip',
                'datetime',
                'url'
            ], 'fields_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->string('url')->change();
            $table->string('user_agent')->change();
            $table->date('datetime')->change();
            $table->dropUnique('fields_unique');
        });
    }
};
