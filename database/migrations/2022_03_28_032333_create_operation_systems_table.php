<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_systems', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->unsignedBigInteger('operation_system_id')->nullable();
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->foreign('operation_system_id')->references('id')->on('operation_systems');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->dropForeign('logs_operation_system_id_foreign');
            $table->dropColumn('operation_system_id');
        });
        Schema::drop('operation_systems');
    }
};
