<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('browsers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->unsignedBigInteger('browser_id')->nullable();
        });
        Schema::table('logs', function (Blueprint $table) {
            $table->foreign('browser_id')->references('id')->on('browsers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->dropForeign('logs_browser_id_foreign');
            $table->dropColumn('browser_id');
        });
        Schema::drop('browsers');
    }
};
