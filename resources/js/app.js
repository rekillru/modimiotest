jQuery(function() {
    var dateFrom = jQuery('input[name=dateFrom]');
    var dateTo = jQuery('input[name=dateTo]');

    dateFrom.datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true,
        language: 'ru'
    })
        .on('changeDate', function(selected){
            var startDate = new Date(selected.date.valueOf());
            var endDate = new Date(selected.date.valueOf());
            var endDate = new Date(endDate.setUTCFullYear(endDate.getUTCFullYear() + 1));
            dateTo.datepicker('setStartDate', startDate);
            dateTo.datepicker('setEndDate', endDate);
        })
        .on('clearDate', function (selected) {
            dateFrom.datepicker('setStartDate', null);
            dateFrom.datepicker('setEndDate', null);
        });

    dateTo.datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true,
        language: 'ru'
    })
        .on('changeDate', function (selected) {
            var endDate = new Date(selected.date.valueOf());
            var startDate = new Date(selected.date.valueOf());
            var startDate = new Date(startDate.setFullYear(startDate.getFullYear() - 1));
            dateFrom.datepicker('setStartDate', startDate);
            dateFrom.datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
            dateFrom.datepicker('setStartDate', null);
            dateFrom.datepicker('setEndDate', null);
        });
});
