<h3>Логи</h3>
<table class="table table-condensed table-hover">
    <thead>
        <tr>
            <th>
                <a href="{{ request()->fullUrlWithQuery(['sort' => 'date']) }}" class="btn btn-link">Дата</a> @if (request()->get('sort') == 'date') &uarr; @endif
            </th>
            <th>
                <a href="{{ request()->fullUrlWithQuery(['sort' => 'request_count']) }}" class="btn btn-link">Число запросов</a> @if (request()->get('sort') == 'request_count') &uarr; @endif
            </th>
            <th>
                <a href="{{ request()->fullUrlWithQuery(['sort' => 'popular_url']) }}" class="btn btn-link">Популярный URL</a> @if (request()->get('sort') == 'popular_url') &uarr; @endif
            </th>
            <th>
                <a href="{{ request()->fullUrlWithQuery(['sort' => 'popular_browser']) }}" class="btn btn-link">Популярный браузер</a> @if (request()->get('sort') == 'popular_browser') &uarr; @endif
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($logsForPage as $log)
            <tr>
                <td>{{ $log->date }}</td>
                <td>{{ $log->request_count }}</td>
                <td>{{ $log->popular_url }} <small class="text-muted">(Запросов: {{ $log->url_count }})</small></td>
                <td>{{ $log->popular_browser_name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
{{ $logsForPage->links() }}
