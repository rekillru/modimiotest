<h3>Графики</h3>
<div class="row">
    <div class="col-md-6">
        <canvas id="request_count_chart"></canvas>
    </div>
    <div class="col-md-6">
        <canvas id="popular_browser_chart"></canvas>
    </div>
</div>
<script>
    new Chart('request_count_chart', {
        type: 'line',
        data: {
            labels: {!! $requestCountChart->keys()->toJson() !!},
            datasets: [{
                label: 'Количество запросов',
                data: {!! $requestCountChart->values()->toJson() !!},
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    new Chart('popular_browser_chart', {
        type: 'line',
        data: {
            labels: {!! $browserChart['dates'] !!},
            datasets: [
            @foreach ($browserChart['browser_names'] as $browserName)
                {
                    label: '{{ $browserName }}',
                    data: {!! $browserChart['request_count'][$browserName]->toJson() !!},
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(7, 162, 5, 0.2)',
                        'rgba(54, 5, 235, 0.2)',
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(7, 162, 5, 1)',
                        'rgba(54, 5, 235, 1)',
                    ],
                    borderWidth: 1
                },
            @endforeach
            ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>
