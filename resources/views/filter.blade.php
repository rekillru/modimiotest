<h3>Фильтр</h3>
<form action="" method="get">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="date">Дата</label>
                <div class="input-group input-daterange">
                    <input type="text" class="form-control" name="dateFrom" value="{{ request()->get('dateFrom') }}" autocomplete="off">
                    <div class="input-group-addon">–</div>
                    <input type="text" class="form-control" name="dateTo" value="{{ request()->get('dateTo') }}" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="operation_system">Операционная система</label>
                <select name="operation_system" id="operation_system" class="form-control">
                    <option value=""></option>
                    @foreach ($operationSystems as $operationSystemID => $operationSystemName)
                        <option value="{{ $operationSystemID }}" @selected(request()->get('operation_system') == $operationSystemID)>{{ $operationSystemName }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="architecture">Архитектура</label>
                <select name="architecture" id="architecture" class="form-control">
                    <option value=""></option>
                    <option value="x86" @selected(request()->get('architecture') == 'x86')>x86</option>
                    <option value="x64" @selected(request()->get('architecture') == 'x64')>x64</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <br>
            <button type="submit" class="btn btn-primary">Найти</button>
            <a href="/" class="btn btn-default">Очистить</a>
        </div>
    </div>

</form>
