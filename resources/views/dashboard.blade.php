@extends('app')

@section('content')
    @include('filter')
    @include('charts')
    @include('logtable')
@endsection
